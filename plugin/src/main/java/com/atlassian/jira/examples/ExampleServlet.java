package com.atlassian.jira.examples;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.JiraRestClientFactory;
import com.atlassian.jira.rest.client.domain.User;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.util.concurrent.Promise;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * A simple Java Servlet that triggers a REST call back to JIRA when the servlet URL is accessed. This is a slightly
 * contrived example, as it is not efficient to use REST to communicate with the JIRA instance that the plugin is
 * actually deployed on (you should use the JIRA Java API instead -
 * https://developer.atlassian.com/display/JIRADEV/JIRA+Java+API+Reference).
 *
 * The URL to access this servlet is defined in the atlassian-plugin.xml file.
 */
public class ExampleServlet extends HttpServlet
{
    private final ApplicationProperties applicationProperties;
    private final UserManager userManager;

    public ExampleServlet(final ApplicationProperties applicationProperties, final UserManager userManager)
    {
        this.applicationProperties = applicationProperties;
        this.userManager = userManager;
    }


    /**
     * To trigger this code, go to http://localhost:2990/jira/plugins/servlet/jrjc (assuming you are running JIRA using
     * the Atlassian Plugin SDK, and you have not customised the plugin at all).
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // We need to know the Base URL of the JIRA instance that we're running in. Use ApplicationProperties for this.
        String jiraBaseURL = applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);

        // We need to authenticate in order to use the JIRA REST API. Let's just assume there is an administrative user
        // called "admin", with a password also set to "admin". This will always be the case if the plugin is run using
        // the Atlassian Plugin SDK. If this is not the case in your JIRA instance, you will need to change these values.
        final String jiraUser = "admin";
        final String jiraPassword = "admin";

        // Setup the JRJC
        JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        URI jiraServerUri;
        try
        {
            jiraServerUri = new URI(jiraBaseURL);
        }
        catch (URISyntaxException e)
        {
            throw new ServletException("Could not understand the JIRA Base URL: " + e.getMessage(), e);
        }

        JiraRestClient client = factory.createWithBasicHttpAuthentication(jiraServerUri, jiraUser, jiraPassword);
        String userToRetrieve = userManager.getRemoteUsername();
        if (StringUtils.isBlank(userToRetrieve))
            userToRetrieve = jiraUser;

        Promise<User> promise = client.getUserClient().getUser(userToRetrieve);

        User user = promise.claim();
        resp.addHeader("Content-Type", "text/plain");
        resp.getWriter().write("The email address of " + userToRetrieve + " is " + user.getEmailAddress());
    }
}
