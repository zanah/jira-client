Client docs: https://ecosystem.atlassian.net/wiki/display/JRJC/Home

This repository demonstrates how to use the JIRA REST Java Client ("JRJC") in a variety of scenarios. The example code is licensed under the terms of the Apache 2.0 License (see LICENSE.txt).

## Stand-alone Example ##

Look in the *standalone* folder for an example of using the JRJC within a stand-alone Java application (for example, an executable JAR file).

### To Build the Standalone Example ###

1. You need Maven 3.0.x installed (I am using 3.0.5).
2. Run the following commands:

		cd standalone/
		mvn clean package

### To Run the Stand-alone Example ###

1. Compile the example using the above instructions.
2. Download and install the Atlassian Plugin SDK if you don't already have it (this is only necessary as a way of easily starting a local JIRA instance to test against)
	Instructions to do this are here: https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project
3. Run a local JIRA instance using the Plugin SDK, by running the following command:

		atlas-run-standalone --product jira --version 6.0 --data-version 6.0

4. Run the Stand-alone JAR by running the following command:

		cd standalone/
		java -jar target/jrjc-example-client-1.0-SNAPSHOT.jar

5. You should see some output like this:

		*********************************************************************************************
		* JIRA Java REST Client ('JRJC') example.                                                    *
		* NOTE: Start JIRA using the Atlassian Plugin SDK before running this example.               *
		* (for example, use 'atlas-run-standalone --product jira --version 6.0 --data-version 6.0'.) *
		**********************************************************************************************
		
		Logging in to http://localhost:2990/jira with username 'admin' and password 'admin'
		Your admin user's email address is: admin@example.com
		
		Example complete. Now exiting.

## Plugin Example ##

Look in the *plugin* folder for an example of using the JRJC within a JIRA plugin. You might do this if you want to integrate a JIRA plugin with another remote JIRA instance.

### To Build the Plugin Example ###

1. You need to install the Atlassian Plugin SDK (https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
2. Run the following commands:

		cd plugin/
		atlas-package
	
### To Run the Plugin Example

1. You need to install the Atlassian Plugin SDK (https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
2. Run the following commands:

		cd plugin/
		atlas-run
	
3. Once JIRA is running, access the following URL in a browser: http://localhost:2990/jira/plugins/servlet/jrjc
4. You should see some output like this:

		The email address of admin is admin@example.com